require 'test_helper'

class MainControllerTest < ActionController::TestCase
  test "should get accueil" do
    get :accueil
    assert_response :success
  end

  test "should get bibliotheque" do
    get :bibliotheque
    assert_response :success
  end

  test "should get ecole" do
    get :ecole
    assert_response :success
  end

  test "should get formations" do
    get :formations
    assert_response :success
  end

  test "should get services" do
    get :services
    assert_response :success
  end

end
