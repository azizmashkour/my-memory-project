require 'test_helper'

class MemoiresControllerTest < ActionController::TestCase
  setup do
    @memoire = memoires(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:memoires)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create memoire" do
    assert_difference('Memoire.count') do
      post :create, memoire: { annee: @memoire.annee, auteur: @memoire.auteur, email: @memoire.email, fichier: @memoire.fichier, filiere: @memoire.filiere, theme: @memoire.theme }
    end
    assert_redirected_to memoire_path(assigns(:memoire))
  end

  test "should show memoire" do
    get :show, id: @memoire
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @memoire
    assert_response :success
  end

  test "should update memoire" do
    patch :update, id: @memoire, memoire: { annee: @memoire.annee, auteur: @memoire.auteur, email: @memoire.email, fichier: @memoire.fichier, filiere: @memoire.filiere, theme: @memoire.theme }
    assert_redirected_to memoire_path(assigns(:memoire))
  end

  test "should destroy memoire" do
    assert_difference('Memoire.count', -1) do
      delete :destroy, id: @memoire
    end

    assert_redirected_to memoires_path
  end
end
