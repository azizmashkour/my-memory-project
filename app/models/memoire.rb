class Memoire < ActiveRecord::Base
  include PgSearch

  belongs_to :user
  has_many :comments
  has_many :likes
  belongs_to :option

  pg_search_scope :search, :against => [:theme, :auteur, :annee, :ecole, :binome]
  paginates_per 9
  has_attached_file :fichier,
   :default_style => :thumb,
   :url => "/:class/:id/:basename.:extension",
   :path => ":rails_root/public/:class/:id/:basename.:extension"

  validates_attachment :fichier, content_type: { content_type: "application/pdf" }

  scope :top_memoires, -> { order(view_number: :desc).limit(6) }

  scope :plus_voter, -> { joins(:likes).order("count_like DESC").select('memoires.*, COUNT(likes.id) AS count_like').group("memoires.id").limit(6) }
end
