class Option < ActiveRecord::Base
  belongs_to :filiere
  has_many :memoires

  def count_memoires
    Memoire.joins(option: {filiere: :diplome}).where("options.id = ?", id).count
  end
end
