class Diplome < ActiveRecord::Base
  has_many :filieres

  def count_memoires
    Memoire.joins(option: {filiere: :diplome}).where("diplomes.id = ?", id).count
  end
end
