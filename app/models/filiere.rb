class Filiere < ActiveRecord::Base
  has_many :options
  belongs_to :diplome

  def count_memoires
    Memoire.joins(option: {filiere: :diplome}).where("filieres.id = ?", id).count
  end
end
