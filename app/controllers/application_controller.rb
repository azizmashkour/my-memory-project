class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alerte => exception.message
  end

  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).concat([:nom, :prenom, :profession, :universite, :diplome, :contact, :filiere])
    devise_parameter_sanitizer.for(:sign_in).concat([:email])
    devise_parameter_sanitizer.for(:account_update).concat([:nom, :prenom, :profession, :universite, :diplome, :contact, :filiere])
  end
end
