class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  # def index
  #   @contacts = Contact.all
  # end

  # GET /contacts/1
  # GET /contacts/1.json
  # def show
  # end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  # def edit
  # end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)
    options = {
      icon_url: "#{ENV['APP_URL']}/scuilt_cask.png",
      username: 'Scuilt',
      channel: '#contacts'
    }
    @slack_poster = Slack::Poster.new('https://hooks.slack.com/services/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', options)
    respond_to do |format|
      if @contact.save
        @slack_poster.send_message("Vous avez un nouveau message de #{@contact.email}: #{@contact.message}")
        format.html { redirect_to new_contact_path, notice: 'Votre message a été envoyé avec succès. Nous vous reviendrons très bientôt!' }
        format.json { render :new, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  # def update
  #   respond_to do |format|
  #     if @contact.update(contact_params)
  #       format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @contact }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @contact.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  # def destroy
  #   @contact.destroy
  #   respond_to do |format|
  #     format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:full_name, :email, :message)
    end
end
