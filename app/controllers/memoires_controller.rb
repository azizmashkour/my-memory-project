## Memoires controller:
class MemoiresController < ApplicationController
  before_action :set_memoire, only: [:show, :edit, :update, :destroy, :download]
  before_action :authenticate_user!, except: [:download, :index, :show]

  def index
    if params[:search]
      @memoires = Memoire.search(params[:search]).page(params[:page]).order("updated_at desc")
    else
      @memoires = current_user.memoires.page(params[:page]).order('updated_at desc')
    end
  end

  def show
    @memoire = Memoire.find(params[:id])
    if @memoire.user_id.present?
      @count = @memoire.update_attribute("view_number", @memoire.view_number + 1)
      @memoire.save
    else
      flash[:error] = 'Désolé! Ce mémoire a été supprimé par son auteur !'
      redirect_to root_path
    end
  end

  def new
    @memoire = Memoire.new
  end

  def edit
    authorize! :update, @memoire
  end

  def create
    @memoire = Memoire.new(memoire_params)
    @memoire.user_id = current_user.id
    options = {
      icon_url: "#{ENV['APP_URL']}/scuilt_cask.png",
      username: 'Scuilt',
      channel: '#memoires'
    }
    @slack_poster = Slack::Poster.new('https://hooks.slack.com/services/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', options)

    respond_to do |format|
      if @memoire.save
        @slack_poster.send_message("Un nouveau theme: #{@memoire.theme} sur Scuilt par #{@memoire.user.email}")
        format.html { redirect_to @memoire, notice: 'Mémoire créé avec succès' }
        format.json { render :show, status: :created, location: @memoire }
      else
        format.html { render :new }
        format.json { render json: @memoire.errors, status: :unprocessable_entity }
      end
    end
  end

  def download
    send_file @memoire.fichier.path, :type => 'application/pdf'
    flash[:notice] = "Votre éléchargement est en cours !"
  end

  def update
    respond_to do |format|
      if @memoire.update(memoire_params)
        format.html { redirect_to @memoire, notice: 'Votre mémorie a été modifié avec succès !' }
        format.json { render :show, status: :ok, location: @memoire }
      else
        format.html { render :edit }
        format.json { render json: @memoire.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @memoire.destroy
    respond_to do |format|
      format.html { redirect_to memoires_url, notice: 'Votre mémorie a été suprimé avec succès !' }
      format.json { head :no_content }
    end
  end

  def like
    @memoire = Memoire.find(params[:id])
    begin
      @like = @memoire.likes.create(user_id: current_user.id, memoire_id: @memoire.id)
      flash[:success] = 'Votre vôte a été pris en compte !'
    rescue
      flash[:error] = 'Désolé! Vous aviez déjà voté pour ce mémoire.'
    end
    redirect_to root_path
  end

  private

    def set_memoire
      @memoire = Memoire.find(params[:id])
    end

    def memoire_params
      params.require(:memoire).permit(:theme, :auteur, :annee, :maitre, :fichier,:binome, :ecole, :option_id, :diplome_id)
    end
end
