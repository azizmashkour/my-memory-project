## HomePage Controller
class MainController < ApplicationController::MemoiresController

  before_action :set_memoire, only: [:show, :edit, :update, :destroy, :download]
  before_action :authenticate_user!, except: [:download, :index, :show, :accueil, :contacts, :recherche]

  def accueil
    @memoires = Memoire.joins(option: {filiere: :diplome})

    if params[:search]
      @memoires = @memoires.search(params[:search])
    end

    if params[:filieres]
      @memoires = @memoires.where("filieres.id IN (?)", params[:filieres])
    end

    if params[:diplomes]
      @memoires = @memoires.where("diplomes.id IN (?)", params[:diplomes])
    end

    if params[:options]
      @memoires = @memoires.where("options.id IN (?)", params[:options])
    end
    @diplomes = Diplome.all

    @memoires = @memoires.order("updated_at desc").page(params[:page])
  end

  def politics
  end

  def termes
  end

  def contacts
  end

  def accounts
  end

  def recherche
    @memoires = Memoire.joins(option: {filiere: :diplome})

    if params[:search]
      @memoires = @memoires.search(params[:search])
    end

    if params[:filieres]
      @memoires = @memoires.where("filieres.id IN (?)", params[:filieres])
    end

    if params[:diplomes]
      @memoires = @memoires.where("diplomes.id IN (?)", params[:diplomes])
    end

    if params[:options]
      @memoires = @memoires.where("options.id IN (?)", params[:options])
    end

    if params[:annees]
      @memoires = @memoires.where(annee: params[:annees])
    end

    if params[:ecoles]
      @memoires = @memoires.where(ecole: params[:ecoles])
    end

    @memoires = @memoires.page(params[:page])

    @diplomes = Diplome.all
    @filieres = Filiere.all
    @options = Option.all
    @annees = Memoire.group(:annee).count
    @ecoles = Memoire.group(:ecole).count
  end
end
