class CommentsController < ApplicationController
  before_action :set_memoire, only: [:show, :edit, :update, :destroy, :download]
  before_action :authenticate_user!, except: [:download, :index, :show]

  def create
    @memoire = Memoire.find(params[:memoire_id])
    @comment = @memoire.comments.create(body: params[:comment][:body], user_id: current_user.id)
    respond_to do |format|
      if @comment
        format.html { redirect_to @memoire, notice: 'Votre commentaire a été enrégistré avec succès !' }
        format.json { render text: @comment.body, status: :created, location: @comment }
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end

  end
end
