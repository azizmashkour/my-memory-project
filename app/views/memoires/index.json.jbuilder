json.array!(@memoires) do |memoire|
  json.extract! memoire, :id, :theme, :auteur, :filiere, :annee, :email, :fichier
  json.url memoire_url(memoire, format: :json)
end
