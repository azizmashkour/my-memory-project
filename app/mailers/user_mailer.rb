class UserMailer < ApplicationMailer
  default from: 'memoire.contacts@gmail.com'

  def welcome_email(user)
    @user = user
    @url  = 'http://memoireapp.herokuapp.com/login'
    mail(to: @user.email, subject: 'Bienvenue sur memoireapp. Nous sommes content de vous compter parmis nos utilisateurs.')
  end
end
