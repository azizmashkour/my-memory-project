class ApplicationMailer < ActionMailer::Base
  default from: "memoire.contacts@gmail.com"
  layout 'mailer'
end
