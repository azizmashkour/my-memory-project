class CreerSerializer < ActiveModel::Serializer
  attributes :id, :themes, :auteurs, :filieres, :annees, :emails
end
