class MemoireSerializer < ActiveModel::Serializer
  attributes :id, :theme, :auteur, :filiere, :annee, :email, :fichier
end
