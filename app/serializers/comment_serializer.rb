class CommentSerializer < ActiveModel::Serializer
  attributes :id, :body, :author, :post_id
end
