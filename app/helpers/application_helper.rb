module ApplicationHelper
   # Returns the full title on a per-page basis.
  def full_title(page_title = 'Partager vos mémoires avec le monde!')
    base_title = "Scuilt"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def is_active(action)
    params[:action] == action ? "active" : nil
  end
end
