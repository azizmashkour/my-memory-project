module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-danger"> <button type="button"
    class="close" data-dismiss="alert">×</button>
      #{messages}
    </div>
    HTML

    html.html_safe
  end

  def devise_error_messages?
    defined?(resource) && !resource.errors.empty?
  end

  private

  def messages_list
    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    "<ul>#{messages}</ul>"
  end
end
