# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161028231048) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.string   "body"
    t.integer  "user_id"
    t.integer  "memoire_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["memoire_id"], name: "index_comments_on_memoire_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "full_name"
    t.string   "email"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "diplomes", force: :cascade do |t|
    t.string "nom"
  end

  create_table "filieres", force: :cascade do |t|
    t.string  "nom"
    t.integer "diplome_id"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "memoire_id"
  end

  add_index "likes", ["user_id", "memoire_id"], name: "index_likes_on_user_id_and_memoire_id", unique: true, using: :btree

  create_table "memoires", force: :cascade do |t|
    t.string   "theme"
    t.string   "ecole"
    t.text     "binome"
    t.string   "auteur"
    t.integer  "annee"
    t.string   "email"
    t.string   "fichier_file_name"
    t.string   "fichier_content_type"
    t.integer  "fichier_file_size"
    t.datetime "fichier_updated_at"
    t.integer  "user_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "option_id"
    t.integer  "view_number",          default: 0
    t.string   "maitre"
  end

  add_index "memoires", ["option_id"], name: "index_memoires_on_option_id", using: :btree
  add_index "memoires", ["user_id"], name: "index_memoires_on_user_id", using: :btree

  create_table "options", force: :cascade do |t|
    t.string  "nom"
    t.integer "filiere_id"
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "pg_search_documents", ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "remember_digest"
    t.string   "nom"
    t.string   "prenom"
    t.string   "profession"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "admin",                  default: false
    t.string   "diplome"
    t.string   "universite"
    t.string   "filiere"
    t.integer  "contact"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "memoires", "options"
end
