class CreateFilieres < ActiveRecord::Migration
  def up
    create_table :filieres do |t|
      t.string :nom
      t.belongs_to :diplome
    end
  end

  def down
    drop_table :filieres
  end
end
