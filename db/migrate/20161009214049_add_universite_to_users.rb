class AddUniversiteToUsers < ActiveRecord::Migration
  def up
    add_column :users, :universite, :string
  end

  def down
    remove_column :users, :universite
  end
end
