class AddFiliereToUsers < ActiveRecord::Migration
  def up
    add_column :users, :filiere, :string
  end

  def down
    remove_column :users, :filiere
  end
end
