class AddMaitreToMemoires < ActiveRecord::Migration
  def up
    add_column :memoires, :maitre, :string
  end

  def down
    remove_column :memoires, :maitre
  end
end
