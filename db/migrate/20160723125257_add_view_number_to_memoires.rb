class AddViewNumberToMemoires < ActiveRecord::Migration
  def up
    add_column :memoires, :view_number, :integer, default: 0
  end

  def down
    remove_column :memoires, :view_number, default: 0
  end
end
