class CreateComments < ActiveRecord::Migration
  def up
    create_table :comments do |t|
      t.string :body
      t.belongs_to :user, index: true
      t.belongs_to :memoire, index: true

      t.timestamps null: false
    end
  end

  def down
    drop_table :comments
  end 
end
