class AddProfessionToUsers < ActiveRecord::Migration
  def up
    add_column :users, :profession, :string
  end

  def down
    remove_column :users, :profession
  end
end
