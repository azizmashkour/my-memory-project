class CreateContacts < ActiveRecord::Migration
  def up
    create_table :contacts do |t|
      t.string :full_name
      t.string :email
      t.string :message

      t.timestamps null: false
    end
  end

  def down
    drop_table :contacts
  end
end
