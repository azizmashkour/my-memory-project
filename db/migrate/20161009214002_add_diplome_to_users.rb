class AddDiplomeToUsers < ActiveRecord::Migration
  def up
    add_column :users, :diplome, :string
  end

  def down
    remove_column :users, :diplome
  end
end
