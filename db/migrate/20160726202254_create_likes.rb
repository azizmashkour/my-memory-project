class CreateLikes < ActiveRecord::Migration
  def up
    create_table :likes do |t|
      t.belongs_to :user
      t.belongs_to :memoire
    end

    add_index :likes, [:user_id, :memoire_id], :unique => true
  end

  def down
    drop_table :likes
  end
end
