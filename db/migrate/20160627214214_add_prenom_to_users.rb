class AddPrenomToUsers < ActiveRecord::Migration
  def up
    add_column :users, :prenom, :string
  end

  def down
    remove_column :users, :prenom
  end
end
