class AddNomToUsers < ActiveRecord::Migration
  def up
    add_column :users, :nom, :string
  end

  def down
    remove_column :users, :nom
  end
end
