class AddOptionToMemoires < ActiveRecord::Migration
  def up
    add_reference :memoires, :option, index: true
    add_foreign_key :memoires, :options
  end

  def down
    remove_column :memoires, :option_id
  end
end
