class CreateMemoires < ActiveRecord::Migration
  def up
    create_table :memoires do |t|
      t.string :theme
      t.string :ecole
      t.text :description
      t.string :auteur
      t.integer :annee
      t.string :email
      t.attachment :fichier
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end

  def down
    drop_table :memoires
  end
end
