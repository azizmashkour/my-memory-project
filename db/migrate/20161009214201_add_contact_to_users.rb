class AddContactToUsers < ActiveRecord::Migration
  def up
    add_column :users, :contact, :integer
  end

  def down
    remove_column :users, :contact
  end
end
