class CreateDiplomes < ActiveRecord::Migration
  def up
    create_table :diplomes do |t|
      t.string :nom
    end
  end

  def down
    drop_table :diplomes
  end
end
