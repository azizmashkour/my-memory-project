class FixColumnName < ActiveRecord::Migration
  def up
    rename_column :memoires, :description, :binome
  end

  def down
    remove_column :memoires, :binome
  end
end
