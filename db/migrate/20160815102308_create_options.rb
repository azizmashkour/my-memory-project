class CreateOptions < ActiveRecord::Migration
  def up
    create_table :options do |t|
      t.string :nom
      t.belongs_to :filiere
    end
  end

  def down
    drop_table :options
  end
end
